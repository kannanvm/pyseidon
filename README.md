# Pyseidon

An ASCII to Unicode converter using Libindic Payyans.

Pyseidon works using Pyside has python3 dependancy.


**To install  Pyseidon**

```
git clone https://gitlab.com/kannanvm/pyseidon.git
cd Pyseidon
```


***To install other requirements***

```
pip3 install -r requirements.txt
```

***To run Pyseidon***

```
python3 Pyseidon.py
```
